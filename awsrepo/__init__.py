import boto
import boto.dynamodb
from ConfigParser import SafeConfigParser
from awsrepo.utils import deb
from awsrepo.utils.indexer import Indexer
from awsrepo.datastore import DynamoDBDataStore
from awsrepo.model import Release, Package, PackageInRelease
from awsrepo.filestore import S3Writer


class AwsRepo(object):

    def __init__(self, config):
        """
        :param config:
        :type config: SafeConfigParser
        """
        self.__config = config
        access_key_id = config.get('aws', 'access_key_id')
        secret_access_key = config.get('aws', 'secret_access_key')
        region = config.get('aws', 'region')
        prefix = config.get('repo', 'db_prefix')
        bucket = config.get('repo', 'bucket_name')

        db = boto.dynamodb.connect_to_region(
            region,
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key)

        self.__data = DynamoDBDataStore(db, prefix)
        self.__writer = S3Writer(
            bucket=bucket,
            access_key_id=access_key_id,
            secret_access_key=secret_access_key)

    def update_index(self, codename):
        gpg_home = self.__config.get('gpg', 'homedir')
        gpg_binary = self.__config.get('gpg', 'binary')

        gpg_keyid = self.__config.get('repo', 'gpg_keyid')
        gpg_passphrase = self.__config.get('repo', 'gpg_passphrase')

        release = self.get_release(codename)
        packages = self.list_packages_in_release(codename)
        indexer = Indexer(release, packages)
        index = indexer.index()
        self.__writer.write_index(
            index,
            gpg_keyid=gpg_keyid,
            gpg_home=gpg_home,
            gpg_binary=gpg_binary,
            gpg_passphrase=gpg_passphrase)

    # --------------------
    # release api
    # --------------------

    def create_release(self, release):
        if self.has_release(release.codename):
            raise AssertionError('release %s already exists.' % release.codename)

        self.__data.add_release(release)

    def has_release(self, codename):
        release = self.__data.get_release(codename)
        return not release is None

    def list_releases(self):
        return self.__data.list_releases()

    def get_release(self, codename):
        return self.__data.get_release(codename)

    def update_release(self, release):
        self.__data.update_release(release)

    # --------------------
    # package api
    # --------------------

    def publish_packages(self, *args):
        packages = [(deb.parse_deb(arg),arg) for arg in args]
        for package in packages:
            self.__data.add_package(package[0])
            with open(package[1]) as f:
                self.__writer.put_package(package[0], f)
        return map((lambda p: p[0]), packages)

    def list_packages(self):
        return self.__data.list_packages()

    def remove_packages(self, *args):
        for pid in args:
            package = self.get_package(pid)
            if not package is None:
                self.__writer.remove_package(package)
                self.__data.remove_package(pid)


    def get_package(self, package_id):
        return self.__data.get_package(package_id)

    def get_package_file(self, package_id):
        package_obj = self.get_package(package_id)
        return self.__writer.get_package(package_obj) if not package_obj is None else None

    # -----------------------
    # package in release api
    # -----------------------

    def add_package_to_release(self, pid, codename, component):
        self.__data.add_package_to_release(pid, codename, component)

    def remove_package_from_release(self, pid, codename, component):
        self.__data.remove_package_from_release(pid, codename, component)

    def list_packages_in_component(self, codename, component):
        return self.__data.get_packages_in_component(codename, component)

    def list_packages_in_release(self, codename):
        return self.__data.get_packages_in_release(codename)
