import os
import re
import gnupg
import shutil
import tempfile
from StringIO import StringIO
from io import BytesIO, FileIO
from awsrepo.model import Package


class Writer(object):

    def sign(self, release_data, gpg_keyid, gpg_passphrase, gpg_binary='gpg', gpg_home=None):
        gpg = gnupg.GPG(gpgbinary=gpg_binary) \
            if gpg_home is None else gnupg.GPG(gpgbinary=gpg_binary, gnupghome=gpg_home)

        sio = StringIO()
        data = gpg.sign(release_data, detach=True, keyid=gpg_keyid, passphrase=gpg_passphrase)
        sio.write(data.data)
        sio.seek(0)
        return sio

    def get_keys(self, gpg_keyid, gpg_binary='gpg', gpg_home=None):
        gpg = gnupg.GPG(gpgbinary=gpg_binary) \
            if gpg_home is None else gnupg.GPG(gpgbinary=gpg_binary, gnupghome=gpg_home)

        # generate pubkey.gpg
        gpg_pub_key = gpg.export_keys(gpg_keyid)
        sio = StringIO()
        sio.write(gpg_pub_key)
        sio.seek(0)

        tempdir = tempfile.mkdtemp()

        # generate keyring.gpg
        gpg_pubring = gnupg.GPG(gnupghome=tempdir, gpgbinary=gpg_binary, keyring='keyring.gpg')
        gpg_pubring.import_keys(gpg_pub_key)

        keyring = BytesIO()
        with open(os.path.join(tempdir, 'keyring.gpg'), 'r+b') as keyring_file:
            keyring.write(keyring_file.read())

        keyring.seek(0)
        shutil.rmtree(tempdir)

        return sio, keyring

    def get_pool_path(self, package_obj):
        """ Creates path parts for the given package
        :param package_obj:
        :type package_obj: Package
        :return: Path parts as a list
        :rtype: list
        """
        return [
            package_obj.name[:4] if re.match(r'^lib.*', package_obj.name) else package_obj.name[:1],
            package_obj.name,
            '{}.deb'.format(str(package_obj))
        ]


class DiskWriter(Writer):

    def __init__(self, location):
        self.__location = location

    def write_index(self, index, gpg_keyid, gpg_passphrase, gpg_binary='gpg', gpg_home=None):
        release_name = index.release[0]
        release_sio = index.release[1]

        dist_dir = os.path.join(self.__location, 'dists', release_name)
        if not os.path.exists(dist_dir):
            os.makedirs(dist_dir, 0755)

        # write dist release file
        with open(os.path.join(dist_dir, 'Release'), 'w') as r_file:
            r_file.write(release_sio.read())

        signed_release = self.sign(
            release_sio.getvalue(), gpg_keyid=gpg_keyid,
            gpg_passphrase=gpg_passphrase, gpg_binary=gpg_binary, gpg_home=gpg_home)

        # write signed dist release file
        with open(os.path.join(dist_dir, 'Release.gpg'), 'w') as signed_r_file:
            signed_r_file.write(signed_release.read())

        keys = self.get_keys(gpg_keyid, gpg_binary, gpg_home)

        # write pubkey.gpg
        with open(os.path.join(self.__location, 'pubkey.gpg'), 'w') as pubkey_file:
            pubkey_file.write(keys[0].read())

        # write keyring.gpg
        with open(os.path.join(self.__location, 'keyring.gpg'), 'w+b') as keyring_file:
            keyring_file.write(keys[1].read())


        archs = index.arch
        for arch in archs:
            comp = arch[0]
            name = arch[1]
            arch_sio = arch[3]
            arch_dir = os.path.join(dist_dir, comp, 'binary-'+name)
            if not os.path.exists(arch_dir):
                os.makedirs(arch_dir, 0755)
            with open(os.path.join(arch_dir, 'Release'), 'w') as arch_file:
                arch_file.write(arch_sio.getvalue())

        packages = index.packages
        for pkg in packages:
            comp = pkg[0]
            arch = pkg[1]
            pkg_sio = pkg[3]
            pkg_gz = pkg[4]
            pkg_dir = os.path.join(dist_dir, comp, 'binary-'+arch)
            if not os.path.exists(pkg_dir):
                os.makedirs(pkg_dir, 0755)
            with open(os.path.join(pkg_dir, 'Packages'), 'w') as pkg_file:
                pkg_file.write(pkg_sio.getvalue())
            with open(os.path.join(pkg_dir, 'Packages.gz'), 'w+b') as pkg_gz_file:
                pkg_gz_file.write(pkg_gz.read())

    def put_package(self, package_obj, package_file):
        """
        :param package_obj:
        :type package_obj: Package
        :param package_file:
        :type package_file: FileIO
        """
        path = self.get_pool_path(package_obj)
        path_str = os.path.join(*path)
        path_str = os.path.join(self.__location, 'pool', path_str)
        package_dir = os.path.dirname(path_str)
        if not os.path.exists(package_dir):
            os.makedirs(package_dir)
        package_file.seek(0)
        with open(path_str, 'w+b') as f:
            f.write(package_file.read())

    def get_package(self, package_obj):
        path = self.get_pool_path(package_obj)
        path_str = os.path.join(*path)
        path_str = os.path.join(self.__location, 'pool', path_str)
        return FileIO(path_str, 'r')


import boto
from boto.s3.key import Key


class S3Writer(Writer):

    def __init__(self, bucket, access_key_id=None, secret_access_key=None):
        self.__bucket = bucket
        self.__access_key_id = access_key_id
        self.__secret_access_key = secret_access_key

    def write_index(self, index, gpg_keyid, gpg_passphrase, gpg_binary='gpg', gpg_home=None):
        release_name = index.release[0]
        release_sio = index.release[1]

        conn = boto.connect_s3(
            aws_access_key_id=self.__access_key_id, 
            aws_secret_access_key=self.__secret_access_key)
        bucket = conn.get_bucket(self.__bucket)

        # write dist release file
        release_key = Key(bucket)
        release_key.key = 'dists/{0}/Release'.format(release_name)
        release_key.set_contents_from_string(release_sio.getvalue())

        # sign release
        signed_release = self.sign(
            release_sio.getvalue(), gpg_keyid=gpg_keyid,
            gpg_passphrase=gpg_passphrase, gpg_binary=gpg_binary, gpg_home=gpg_home)

        # write signed dist release file
        signed_release_key = Key(bucket)
        signed_release_key.key = 'dists/{0}/Release.gpg'.format(release_name)
        signed_release_key.set_contents_from_file(signed_release)

        # get gpg keys
        keys = self.get_keys(gpg_keyid, gpg_binary, gpg_home)

        # write pubkey.gpg
        pubkey_key = Key(bucket)
        pubkey_key.key = 'dists/pubkey.gpg'
        pubkey_key.set_contents_from_file(keys[0])
        pubkey_key.make_public()

        # write keyring.gpg
        keyring_key = Key(bucket)
        keyring_key.key = 'dists/keyring.gpg'
        keyring_key.set_contents_from_file(keys[1])
        keyring_key.make_public()

        arch_release_path = 'dists/{0}/{1}/binary-{2}/Release'
        archs = index.arch
        for arch in archs:
            comp = arch[0]
            name = arch[1]
            arch_sio = arch[3]
            arch_sio.seek(0)
            arch_release_key = Key(bucket)
            arch_release_key.key = arch_release_path.format(release_name, comp, name)
            arch_release_key.set_contents_from_file(arch_sio)

        arch_packages_path = 'dists/{0}/{1}/binary-{2}/Packages'
        arch_packages_gz_path = 'dists/{0}/{1}/binary-{2}/Packages.gz'
        packages = index.packages
        for pkg in packages:
            comp = pkg[0]
            arch = pkg[1]
            pkg_sio = pkg[3]
            pkg_gz = pkg[4]
            pkgs_key = Key(bucket)
            pkgs_key.key = arch_packages_path.format(release_name, comp, arch)
            pkgs_key.set_contents_from_file(pkg_sio)
            pkgs_gz_key = Key(bucket)
            pkgs_gz_key.key = arch_packages_gz_path.format(release_name, comp, arch)
            pkgs_gz_key.set_contents_from_file(pkg_gz)

    def put_package(self, package_obj, package_file):
        """
        :param package_obj:
        :type package_obj: Package
        :param package_file:
        :type package_file: FileIO
        """
        path = self.get_pool_path(package_obj)
        conn = boto.connect_s3(aws_access_key_id=self.__access_key_id, aws_secret_access_key=self.__secret_access_key)
        bucket = conn.get_bucket(self.__bucket)

        # write dist release file
        key = Key(bucket)
        key.key = 'pool/'+'/'.join(path)
        key.set_contents_from_file(package_file)

    def get_package(self, package_obj):
        """
        :param package_obj: package to get from S3
        :type package_obj: Package
        :return: byte buffer containing the package from S3
        :rtype: BytesIO
        """
        path = self.get_pool_path(package_obj)
        conn = boto.connect_s3(aws_access_key_id=self.__access_key_id, aws_secret_access_key=self.__secret_access_key)
        bucket = conn.get_bucket(self.__bucket)
        bio = BytesIO()
        key = Key(bucket)
        key.key = 'pool/'+'/'.join(path)
        key.get_contents_to_file(bio)
        bio.seek(0)
        return bio

    def remove_package(self, package_obj):
        """
        :param package_obj:
        :type package_obj:
        :return:
        :rtype:
        """
        path = self.get_pool_path(package_obj)
        conn = boto.connect_s3(aws_access_key_id=self.__access_key_id, aws_secret_access_key=self.__secret_access_key)
        bucket = conn.get_bucket(self.__bucket)
        key = Key(bucket)
        key.key = 'pool/'+'/'.join(path)
        key.delete()
