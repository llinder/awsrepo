import os
from setuptools import setup, find_packages

# Utility function to read the README file.
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="awsrepo",
    version="0.0.2",
    author="Lance Linder",
    author_email="llinder@gmail.com",
    description=("Utilities for managing Debian repository in S3."),
    license="BSD",
    keywords="s3 debian apt repository",
    url="https://github.com/llinder/s3repo",
    package_data={'awsrepo.templates':['architecture.jinja', 'package.jinja', 'release.jinja']},
 #   packages=['awsrepo','awsrepo.cli'],
    packages=find_packages(exclude=['tests','tests.*']),
    test_suite='tests',
#    scripts=[
#        "scripts/awsrepo-upload",
#        "scripts/awsrepo-delete",
#        "scripts/awsrepo-rebuild",
#        "scripts/awsrepo-list"],
    install_requires=["boto>=2.6.0", 'jinja2', 'python-gnupg'],
    tests_require=['ddbmock','moto']
)
