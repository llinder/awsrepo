from unittest import TestCase
from ddbmock import connect_boto_patch
from awsrepo.datastore import DynamoDBDataStore
from awsrepo.model import Package


class TestPackage(TestCase):

    def setUp(self):
        self.__packages = [
            Package({
                'Package':'foo',
                'Version':'0.1-1',
                'Architecture':'all',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test'
            })
        ]

    def tearDown(self):
        from ddbmock.database.db import dynamodb
        from ddbmock import clean_boto_patch
        dynamodb.hard_reset()
        clean_boto_patch()

    def test_to_string(self):
        p = self.__packages[0]
        self.assertEqual('foo_0.1-1_all', str(p))


    def test_add_package(self):
        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')
        repo.add_package(self.__packages[0])

        packages = repo.list_packages()
        self.assertEqual(1, len(packages), 'there should be one package')

    def test_get_package(self):
        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')
        package = self.__packages[0]
        repo.add_package(package)

        ret = repo.get_package(str(package))
        self.assertEqual(str(package), str(ret))

    def test_add_remove(self):
        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')
        package = self.__packages[0]
        repo.add_package(package)

        count = len(repo.list_packages())
        repo.remove_package(str(package))
        self.assertEqual(len(repo.list_packages()), count-1, 'package should have been removed')