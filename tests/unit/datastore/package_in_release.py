from unittest import TestCase
from ddbmock import connect_boto_patch
from awsrepo.datastore import DynamoDBDataStore
from awsrepo.model import Package, Release


class TestPackageInRevision(TestCase):

    def setUp(self):
        self.__packages = [
            Package({
                'Package':'foo',
                'Version':'0.1-1',
                'Architecture':'all',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test'
            }),
            Package({
                'Package':'bar',
                'Version':'1.5-3',
                'Architecture':'i386',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test'
            }),
            Package({
                'Package':'bash',
                'Version':'0.9-1',
                'Architecture':'amd64',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test'
            })
        ]

        self.__releases = [
            Release({
                'Codename':'precise',
                'Origin':'test',
                'Label':'test repo',
                'Suite':'stable',
                'Components':['main'],
                'Architectures':['i386', 'amd64']
            }),
            Release({
                'Codename':'raring',
                'Origin':'test',
                'Label':'another test repo',
                'Suite':'unstable',
                'Components':['main'],
                'Architectures':['i386', 'amd64']
            })
        ]

        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')

        repo.add_release(self.__releases[0])
        repo.add_release(self.__releases[1])

        repo.add_package(self.__packages[0])
        repo.add_package(self.__packages[1])
        repo.add_package(self.__packages[2])

        self.__repo = repo

    def tearDown(self):
        from ddbmock.database.db import dynamodb
        from ddbmock import clean_boto_patch
        dynamodb.hard_reset()
        clean_boto_patch()

    def test_add(self):
        pid = str(self.__packages[0])
        codename = self.__releases[0].codename
        component = 'main'
        self.__repo.add_package_to_release(pid, codename, component)

        components = self.__repo.get_packages_in_release(codename)
        self.assertTrue('main' in components)
        self.assertEqual(1, len(components['main']))
        self.assertTrue(self.__packages[0] in components['main'])

    def test_remove(self):
        codename = self.__releases[0].codename
        self.__repo.add_package_to_release(str(self.__packages[0]), codename, 'main')
        self.__repo.add_package_to_release(str(self.__packages[1]), codename, 'main')
        self.__repo.add_package_to_release(str(self.__packages[2]), codename, 'main')

        components = self.__repo.get_packages_in_release(codename)

        count = sum(len(components[c]) for c in components)

        self.__repo.remove_package_from_release(str(self.__packages[0]), codename, 'main')

        components = self.__repo.get_packages_in_release(codename)

        count_after = sum(len(components[c]) for c in components)

        self.assertEqual(count_after, count - 1)