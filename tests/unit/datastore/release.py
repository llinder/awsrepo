from awsrepo.datastore import DynamoDBDataStore
from awsrepo.model import Release
from unittest import TestCase
from ddbmock import connect_boto_patch

class TestRelease(TestCase):

    def tearDown(self):
        from ddbmock.database.db import dynamodb
        from ddbmock import clean_boto_patch
        dynamodb.hard_reset()
        clean_boto_patch()

    def test_initialize(self):
        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')
        self.assertIsNotNone(repo.release_table, 'release table should exist')
        self.assertIsNotNone(repo.package_table, 'package table should exist')
        self.assertIsNotNone(repo.package_in_release_table, 'package_in_release table should exist')

    def test_add_release(self):
        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')
        release = Release({
            'Codename':'precise',
            'Origin':'test',
            'Label':'test repo',
            'Suite':'unstable',
            'Components':['main', 'unstable', 'testing'],
            'Architectures':['i386','amd64']
        })
        repo.add_release(release)

        releases = repo.list_releases()
        self.assertEqual(1, len(releases), 'there should be one release')

    def test_get_release(self):
        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')
        release = Release({
            'Codename':'precise',
            'Origin':'test',
            'Label':'test repo',
            'Suite':'unstable',
            'Components':['main', 'unstable', 'testing'],
            'Architectures':['i386','amd64']
        })
        repo.add_release(release)

        release = repo.get_release('precise')

        self.assertIsNotNone(release)
        self.assertEqual('precise', release.codename)

    def test_list_releases(self):
        db = connect_boto_patch()
        repo = DynamoDBDataStore(db, 'test.')
        release = Release({
            'Codename':'precise',
            'Origin':'test',
            'Label':'test repo',
            'Suite':'unstable',
            'Components':['main', 'unstable', 'testing'],
            'Architectures':['i386','amd64']
        })
        repo.add_release(release)

        releases = repo.list_releases()
        self.assertTrue(len(releases) > 0)

