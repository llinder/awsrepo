import os
import tempfile
import shutil
from awsrepo.filestore import DiskWriter
from unittest import TestCase
from awsrepo.model import Package, Release
from awsrepo.utils.indexer import Indexer


class TestDiskWriter(TestCase):

    components = {
        'main':[
            Package({
                'Package':'foo',
                'Version':'0.1-1',
                'Architecture':'all',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':(
                    'just a test\n'
                    'another line'
                ),
                'Size':'1698',
                'MD5sum':'abc',
                'SHA1':'zyx',
                'SHA256':'123'
            }),
            Package({
                'Package':'bar',
                'Version':'1.5-3',
                'Architecture':'i386',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test',
                'Size':'1698',
                'MD5sum':'abc',
                'SHA1':'zyx',
                'SHA256':'123'
            }),
            Package({
                'Package':'bash',
                'Version':'0.9-1',
                'Architecture':'amd64',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test',
                'Size':'1698',
                'MD5sum':'abc',
                'SHA1':'zyx',
                'SHA256':'123'
            })]

    }

    releases = [
        Release({
            'Codename':'test1',
            'Origin':'test',
            'Label':'test repo',
            'Suite':'stable',
            'Components':['main'],
            'Architectures':['i386', 'amd64']
        }),
        Release({
            'Codename':'test2',
            'Origin':'test',
            'Label':'another test repo',
            'Suite':'unstable',
            'Components':['main'],
            'Architectures':['i386', 'amd64']
        })
    ]

    def setUp(self):
        self.__tempdir = tempfile.mkdtemp()
        self.__gpghome = tempfile.mkdtemp()
        pubring = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'pubring.gpg')
        secring = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'secring.gpg')
        trustdb = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'trustdb.gpg')
        shutil.copyfile(pubring, os.path.join(self.__gpghome, 'pubring.gpg'))
        shutil.copyfile(secring, os.path.join(self.__gpghome, 'secring.gpg'))
        shutil.copyfile(trustdb, os.path.join(self.__gpghome, 'trustdb.gpg'))

    def tearDown(self):
        shutil.rmtree(self.__tempdir)
        shutil.rmtree(self.__gpghome)

    def test_put_package(self):
        dep_path = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'foo_0.1-1_all.deb')
        writer = DiskWriter(self.__tempdir)
        with open(dep_path) as f:
            writer.put_package(TestDiskWriter.components['main'][0], f)

        path = os.path.join(self.__tempdir, 'pool', 'f', 'foo', 'foo_0.1-1_all.deb')
        stat = os.stat(path)
        self.assertTrue(os.path.exists(path))
        self.assertTrue(stat.st_size > 0)

    def test_write(self):
        indexer = Indexer(TestDiskWriter.releases[0], TestDiskWriter.components)
        index = indexer.index()
        writer = DiskWriter(self.__tempdir)
        writer.write_index(index, '243659B73288D31D', 'temp123', gpg_binary='/usr/local/bin/gpg', gpg_home=self.__gpghome)

        release_file = os.path.join(self.__tempdir, 'dists', 'test1', 'Release')
        self.assertTrue(os.path.exists(release_file))
        self.assertTrue(os.path.getsize(release_file) > 0)