import os
import tempfile
import shutil
import boto
from moto import mock_s3
from awsrepo.filestore import S3Writer
from unittest import TestCase
from awsrepo.model import Package, Release
from awsrepo.utils.indexer import Indexer
from io import BytesIO

class TestS3Writer(TestCase):

    components = \
        {'main': [
            Package({
                'Package':'foo',
                'Version':'0.1-1',
                'Architecture':'all',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description': (
                    'just a test\n'
                    'another line'
                ),
                'Size':'1698',
                'MD5sum':'abc',
                'SHA1':'zyx',
                'SHA256':'123'
            }),
            Package({
                'Package':'bar',
                'Version':'1.5-3',
                'Architecture':'i386',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test',
                'Size':'1698',
                'MD5sum':'abc',
                'SHA1':'zyx',
                'SHA256':'123'
            }),
            Package({
                'Package':'bash',
                'Version':'0.9-1',
                'Architecture':'amd64',
                'Maintainer':'John Doe <jdoe@awesome.org>',
                'Installed-Size':29,
                'Depends':'bar (>= 1.0), libbar',
                'Section':'misc',
                'Priority':'extra',
                'Homepage':'http://www.theuselessweb.com',
                'Description':'just a test',
                'Size':'1698',
                'MD5sum':'abc',
                'SHA1':'zyx',
                'SHA256':'123'
            })
        ]}

    releases = [
        Release({
            'Codename':'test1',
            'Origin':'test',
            'Label':'test repo',
            'Suite':'stable',
            'Components':['main'],
            'Architectures':['i386', 'amd64']
        }),
        Release({
            'Codename':'test2',
            'Origin':'test',
            'Label':'another test repo',
            'Suite':'unstable',
            'Components':['main'],
            'Architectures':['i386', 'amd64']
        })
    ]

    def setUp(self):
        self.__tempdir = tempfile.mkdtemp()
        self.__gpghome = tempfile.mkdtemp()
        pubring = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'pubring.gpg')
        secring = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'secring.gpg')
        trustdb = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'trustdb.gpg')
        shutil.copyfile(pubring, os.path.join(self.__gpghome, 'pubring.gpg'))
        shutil.copyfile(secring, os.path.join(self.__gpghome, 'secring.gpg'))
        shutil.copyfile(trustdb, os.path.join(self.__gpghome, 'trustdb.gpg'))

    def tearDown(self):
        shutil.rmtree(self.__tempdir)
        shutil.rmtree(self.__gpghome)

    @mock_s3
    def test_put_package(self):
        conn = boto.connect_s3()
        conn.create_bucket('my-apt-bucket')
        dep_path = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'foo_0.1-1_all.deb')
        writer = S3Writer('my-apt-bucket')
        with open(dep_path) as f:
            writer.put_package(TestS3Writer.components['main'][0], f)

        bucket = conn.get_bucket('my-apt-bucket')
        key = bucket.get_key('pool/f/foo/foo_0.1-1_all.deb')
        key_bio = BytesIO()
        key.get_contents_to_file(key_bio)
        self.assertTrue(len(key_bio.getvalue()) > 0)

    @mock_s3
    def test_write(self):
        conn = boto.connect_s3()
        conn.create_bucket('my-apt-bucket')

        indexer = Indexer(TestS3Writer.releases[0], TestS3Writer.components)
        index = indexer.index()
        writer = S3Writer('my-apt-bucket')
        writer.write_index(
            index, '243659B73288D31D', 'temp123', gpg_binary='/usr/local/bin/gpg', gpg_home=self.__gpghome)

        bucket = conn.get_bucket('my-apt-bucket')
        release_key = bucket.get_key('dists/test1/Release')
        release_bio = BytesIO()
        release_key.get_contents_to_file(release_bio)
        self.assertTrue(len(release_bio.getvalue()) > 0, msg='check Release')

        signed_release_key = bucket.get_key('dists/test1/Release.gpg')
        signed_release_bio = BytesIO()
        signed_release_key.get_contents_to_file(signed_release_bio)
        self.assertTrue(len(signed_release_bio.getvalue()) > 0, msg='check Release.gpg')

        pubkey_key = bucket.get_key('dists/pubkey.gpg')
        pubkey_bio = BytesIO()
        pubkey_key.get_contents_to_file(pubkey_bio)
        self.assertTrue(len(pubkey_bio.getvalue()) > 0, msg='check pubkey.gpg')

        keyring_key = bucket.get_key('dists/keyring.gpg')
        keyring_bio = BytesIO()
        keyring_key.get_contents_to_file(keyring_bio)
        self.assertTrue(len(keyring_bio.getvalue()) > 0, msg='check keyring.gpg')

        arch_i386_key = bucket.get_key('dists/test1/main/binary-i386/Release')
        arch_i386_bio = BytesIO()
        arch_i386_key.get_contents_to_file(arch_i386_bio)
        self.assertTrue(len(arch_i386_bio.getvalue()) > 0, msg='check arch i386 Release')

        arch_amd64_key = bucket.get_key('dists/test1/main/binary-amd64/Release')
        arch_amd64_bio = BytesIO()
        arch_amd64_key.get_contents_to_file(arch_amd64_bio)
        self.assertTrue(len(arch_amd64_bio.getvalue()) > 0, msg='check arch amd64 Release')

        packages_i386_key = bucket.get_key('dists/test1/main/binary-amd64/Packages')
        packages_i386_bio = BytesIO()
        packages_i386_key.get_contents_to_file(packages_i386_bio)
        self.assertTrue(len(packages_i386_bio.getvalue()) > 0, msg='check i386 Packages')

        packages_i386_gz_key = bucket.get_key('dists/test1/main/binary-amd64/Packages.gz')
        packages_i386_gz_bio = BytesIO()
        packages_i386_gz_key.get_contents_to_file(packages_i386_gz_bio)
        self.assertTrue(len(packages_i386_gz_bio.getvalue()) > 0, msg='check i386 Packages.gz')