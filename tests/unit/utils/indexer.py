from unittest import TestCase
from StringIO import StringIO
from awsrepo.model import Package, Release
from awsrepo.utils.indexer import Indexer, idx_package, idx_packages, collect, gzip


class TestIndexer(TestCase):

    def setUp(self):

        self.__components = \
            {'main': [
                Package({
                    'Package':'foo',
                    'Version':'0.1-1',
                    'Architecture':'all',
                    'Maintainer':'John Doe <jdoe@awesome.org>',
                    'Installed-Size':29,
                    'Depends':'bar (>= 1.0), libbar',
                    'Section':'misc',
                    'Priority':'extra',
                    'Homepage':'http://www.theuselessweb.com',
                    'Description': (
                        'just a test\n'
                        'another line'
                    ),
                    'Size':'1698',
                    'MD5sum':'abc',
                    'SHA1':'zyx',
                    'SHA256':'123'
                }),
                Package({
                    'Package':'bar',
                    'Version':'1.5-3',
                    'Architecture':'i386',
                    'Maintainer':'John Doe <jdoe@awesome.org>',
                    'Installed-Size':29,
                    'Depends':'bar (>= 1.0), libbar',
                    'Section':'misc',
                    'Priority':'extra',
                    'Homepage':'http://www.theuselessweb.com',
                    'Description':'just a test',
                    'Size':'1698',
                    'MD5sum':'abc',
                    'SHA1':'zyx',
                    'SHA256':'123'
                }),
                Package({
                    'Package':'bash',
                    'Version':'0.9-1',
                    'Architecture':'amd64',
                    'Maintainer':'John Doe <jdoe@awesome.org>',
                    'Installed-Size':29,
                    'Depends':'bar (>= 1.0), libbar',
                    'Section':'misc',
                    'Priority':'extra',
                    'Homepage':'http://www.theuselessweb.com',
                    'Description':'just a test',
                    'Size':'1698',
                    'MD5sum':'abc',
                    'SHA1':'zyx',
                    'SHA256':'123'
                })]
            }

        self.__releases = [
            Release({
                'Codename':'test1',
                'Origin':'test',
                'Label':'test repo',
                'Suite':'stable',
                'Components':['main'],
                'Architectures':['i386', 'amd64']
            }),
            Release({
                'Codename':'test2',
                'Origin':'test',
                'Label':'another test repo',
                'Suite':'unstable',
                'Components':['main'],
                'Architectures':['i386', 'amd64']
            })
        ]

    def test_index(self):
        index = Indexer(self.__releases[0], self.__components)
        result = index.index()
        self.assertIsNotNone(result.release)
        self.assertIsNotNone(result.arch)
        self.assertIsNotNone(result.packages)
        self.assertEqual('test1', result.release[0])

    def test_gzip(self):
        s = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        sio = StringIO()
        sio.write(s)
        self.assertEqual(446, sio.len)
        gsio = gzip(sio)
        self.assertEqual(282, gsio.len)

    def test_idx_package(self):
        #packages = collect(['main'], ['i386', 'amd64'], self.__components)
        index = idx_package(self.__components['main'][0])
        self.assertTrue(len(index) > 0)
        s = ("Package: foo\n"
             "Version: 0.1-1\n"
             "Architecture: all\n"
             "Maintainer: John Doe <jdoe@awesome.org>\n"
             "Depends: bar (>= 1.0), libbar\n"
             "Installed-Size: 29\n"
             "Homepage: http://www.theuselessweb.com\n"
             "Section: misc\n"
             "Description: just a test\n"
             " another line\n"
             "Filename: pool/f/foo/foo_0.1-1_all.deb\n"
             "Size: 1698\n"
             "SHA1: zyx\n"
             "SHA256: 123\n"
             "MD5sum: abc\n"
             "Priority: extra")
        self.assertEqual(s, index)

    def test_idx_packages(self):
        pack_map = collect(['main'], ['i386', 'amd64'], self.__components)
        packages = [p for c in pack_map for p in pack_map[c]['i386']]
        index = idx_packages('main', 'i386', packages)
        self.assertTrue(len(index) > 0)
        s = ("Package: foo\n"
             "Version: 0.1-1\n"
             "Architecture: all\n"
             "Maintainer: John Doe <jdoe@awesome.org>\n"
             "Depends: bar (>= 1.0), libbar\n"
             "Installed-Size: 29\n"
             "Homepage: http://www.theuselessweb.com\n"
             "Section: misc\n"
             "Description: just a test\n"
             " another line\n"
             "Filename: pool/f/foo/foo_0.1-1_all.deb\n"
             "Size: 1698\n"
             "SHA1: zyx\n"
             "SHA256: 123\n"
             "MD5sum: abc\n"
             "Priority: extra\n"
             "\n"
             "Package: bar\n"
             "Version: 1.5-3\n"
             "Architecture: i386\n"
             "Maintainer: John Doe <jdoe@awesome.org>\n"
             "Depends: bar (>= 1.0), libbar\n"
             "Installed-Size: 29\n"
             "Homepage: http://www.theuselessweb.com\n"
             "Section: misc\n"
             "Description: just a test\n"
             "Filename: pool/b/bar/bar_1.5-3_i386.deb\n"
             "Size: 1698\n"
             "SHA1: zyx\n"
             "SHA256: 123\n"
             "MD5sum: abc\n"
             "Priority: extra")
        self.assertEqual('main', index[0])
        self.assertEqual('i386', index[1])
        self.assertEqual(s, index[3].getvalue())