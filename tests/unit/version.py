from unittest import TestCase
from awsrepo.model import Version


class TestUtils(TestCase):

    def test_from_str1(self):
        v = Version.from_str('0.1-1')
        self.assertEqual('0.1', v.version)
        self.assertEqual('1', v.iteration)
        self.assertEqual(None, v.epoch)

    def test_from_str2(self):
        v = Version.from_str('1:0.1-1')
        self.assertEqual('0.1', v.version)
        self.assertEqual('1', v.iteration)
        self.assertEqual('1', v.epoch)

    def test_from_str3(self):
        v = Version.from_str('5')
        self.assertEqual('5', v.version)

    def test_version(self):
        v = Version('1', '1.0', '1')
        self.assertEqual('1', v.epoch)
        self.assertEqual('1.0', v.version)
        self.assertEqual('1', v.iteration)

    def test_version_str(self):
        v = Version('1', '1.0', '1')
        self.assertEqual('1:1.0-1', str(v))

    def test_version_str2(self):
        v = Version(None, '1.0', '1')
        self.assertEqual('1.0-1', str(v))

    def test_version_short(self):
        v = Version('1', '1.0', '1')
        self.assertEqual('1.0-1', v.short_str())